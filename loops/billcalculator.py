"""
Q. Accept total number of bills to store from end user
   based on number of bills accept respective bill amounts
   calculate final bill amount based on following criteria

   Original Amt         Tip Amt         Total
   400                  20              420
   600                  60              660
   ==========================================
   1000                 80              1080

   if bill amount is less than 500 ==> 5% tip
   if bill amount is greater than 500==> 10% tip
"""
# accept total number of bills to be stored from end user
totalBills = int(input("ENTER TOTAL NUMBER OF BILLS\n"))
print("PLEASE ENTER ", totalBills, "BILL AMOUNTS")

# declare all list variables
bill_amounts = []
tip_amounts = []
final_amounts = []


# accept bill amounts from end user
def accept_bill_amounts():
    for bill in range(0, totalBills):
        amt = float(input())
        bill_amounts.append(amt)


# calculate tip for each bill and store in different list
def calculate_tip_amounts():
    accept_bill_amounts()
    for bill in range(0, totalBills):
        if bill_amounts[bill] <= 500:
            tip_amounts.append(bill_amounts[bill] * 0.05)
        else:
            tip_amounts.append(bill_amounts[bill] * 0.1)


# calculate final bill amount including tips
def calculate_final_bill():
    calculate_tip_amounts()
    for bill in range(0, totalBills):
        total = bill_amounts[bill] + tip_amounts[bill]
        final_amounts.append(total)


# calculate sum of all the amounts
def calculate_sum_amounts():
    calculate_final_bill()
    # declare variables to store sum of all the amounts
    original_amount_sum = 0.0
    tip_amount_sum = 0.0
    final_amount_sum = 0.0

    for bill in range(0, totalBills):
        original_amount_sum += bill_amounts[bill]
        tip_amount_sum += tip_amounts[bill]
        final_amount_sum += final_amounts[bill]

    return original_amount_sum, tip_amount_sum, final_amount_sum


# print all the details in given format
def print_bill_details():
    original_amount_sum, tip_amount_sum, final_amount_sum = calculate_sum_amounts()
    print("BILL AMT\tTIP AMT\t\tFINAL AMT")
    print("===============================")

    for bill in range(0, totalBills):
        print(bill_amounts[bill], tip_amounts[bill], final_amounts[bill], sep="\t\t")

    print("===============================")
    print(original_amount_sum, tip_amount_sum, final_amount_sum, sep="\t\t")


# give a call to print details method
print_bill_details()
