"""
Develop an application for grocery shop which contains the list of different grocery items.
customer can purchase the grocery items from the given list. After purchase operation generate
a bill which contains the amount details.

1) Display Grocery Items
2) Add Items to Cart
3) View Cart Items
4) Checkout
5) PayBill
6) Exit
"""

grocery_items = {'SUGAR': 35, 'TEA': 65, 'SEEDS': 50, 'SALT': 25, 'OIL': 150}
cart_items = {}
final_amt = 0


def show_grocery_items():
    print('ITEM\t\t\tPRICE')
    print("====================================")
    for k, v in grocery_items.items():
        print(k, '\t\t\t', v)


def add_items_to_cart(item_name, qty):
    original_item = item_name.upper()
    result = original_item in grocery_items
    if result:
        cart_items[original_item] = grocery_items.get(original_item) * qty
        print("ITEM ADDED SUCCESSFULLY")
    else:
        print("ITEM IS NOT PRESENT")


def view_cart_items():
    print("ITEM\t\t\tU.PRICE\t\t\tTOTAL")
    print("====================================")
    for k, v in cart_items.items():
        print(k, '\t\t\t', grocery_items[k], '\t\t\t', v)


def checkout():
    global final_amt
    total = 0
    print("ITEM\t\t\tU.PRICE\t\t\tTOTAL")
    print("====================================")
    for k, v in cart_items.items():
        print(k, '\t\t\t', grocery_items[k], '\t\t\t', v)
        total += v
    final_amt = total
    print("====================================")
    print("FINAL BILL AMOUNT IS ", final_amt)


def bill_payment(amount):
    global final_amt
    if amount == final_amt:
        final_amt -= amount
        print("PAYMENT SUCCESSFUL")
        cart_items.clear()
    else:
        print("INVALID AMOUNT")


def accept_input():
    print("Welcome to Apna Mart")
    want_to_continue = True
    while want_to_continue:
        print("====================================")
        print("1: View Grocery Items")
        print("2: Add Items to Cart")
        print("3: Display Cart Items")
        print("4: Checkout")
        print("5: Pay bill")
        print("6: Exit")
        choice = int(input("PLEASE ENTER YOUR CHOICE\n"))

        if choice == 1:
            show_grocery_items()
        elif choice == 2:
            item_name = input("ENTER ITEM NAME\n")
            qty = int(input("ENTER QTY\n"))
            add_items_to_cart(item_name, qty)
        elif choice == 3:
            view_cart_items()
        elif choice == 4:
            checkout()
        elif choice == 5:
            amount = float(input("ENTER AMOUNT TO BE PAID\n"))
            bill_payment(amount)

        elif choice == 6:
            if final_amt == 0:
                want_to_continue = False
            else:
                print("PAY YOUR BILL FIRST")
    print("THANKS FOR SHOPPING WITH US. DO COME AGAIN!!!!")


accept_input()
