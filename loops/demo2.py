data = [[10, 20, 30], 'JAVA', 'PYTHON']

for item in data:
    print(item)


def square(no):
    return no ** 2


no_list = [2, 4, 6, 8]
print(list(map(square, no_list)))
